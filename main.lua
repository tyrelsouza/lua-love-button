function love.load()
  gameState = 'menu'

  button = {}
  button.size = 50

  button.x = love.math.random(button.size, love.graphics.getWidth() - button.size)
  button.y = love.math.random(button.size, love.graphics.getHeight() - button.size)

  score = 0
  timer = 10

  myFont = love.graphics.newFont(40)
end
function reset()
  timer = 0
  score = 0
  gameState = "menu"
end

function love.update(dt)
  if gameState == "game" then
    if timer > 0 then
      timer = timer - dt
    end
    if timer < 0 then
      reset()
    end
  end
end

function game()
  love.graphics.setColor(100, 0, 0)
  love.graphics.circle("fill", button.x, button.y, button.size)
end

function love.draw()
  love.graphics.setColor(255, 255, 255)
  love.graphics.setFont(myFont)
  love.graphics.print("Score:" .. score)
  love.graphics.print("Time:" .. math.ceil(timer), 200, 0)

  if gameState == "game" then
    game()
  else
    love.graphics.printf("Click anywhere to begin", 0,  love.graphics.getHeight()/2, love.graphics.getWidth(), "center")
  end

end

function increaseScore()
  score = score + 1
  button.x = love.math.random(button.size, love.graphics.getWidth() - button.size)
  button.y = love.math.random(button.size, love.graphics.getHeight() - button.size)
end


function love.mousepressed(x, y, btn, isTouch)
  if btn == 1 and gameState == "game" then
      if distanceBetween(button.x, button.y, love.mouse.getX(), love.mouse.getY()) < button.size then
        increaseScore()
      end
  end

  if gameState == "menu" then
    gameState = "game"
    timer = 10
  end
end

function distanceBetween(x1, y1, x2, y2)
  return math.sqrt((y2 - y1)^2 + (x2 - x1)^2)
end
